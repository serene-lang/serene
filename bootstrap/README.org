#+OPTIONS:     num:t toc:nil
* Serene lang (Bootstrap interpreter)
The bootstrap version of Serene is used to bootstrap the compiler which is written in Serene
itself. It's an interpreter with minimal set of features and rough edges.

#+TOC: headlines 2

** Heads up
Since the interpreter is there to bootstrap the compiler the goal is to keep it simple and good enough
too run the compiler. So we don't want to over engineer things because the interpreter will go away
evantually. Here is the list of things that we implemented ( or will implement ) differently:

- *Normal Macros* (Syntax macro): Since bootstrap version is an interpreter we don't have clear
distinction between compile time and runtime (We have only runtime) - unless we build bytecode support
into the interpreter to be able to cache the compiled ast - So we can't really implemeent normal macros
as they meant to be implemented.

- List implementation: We don't follow the actual list implementation as a single or doubly linked
list. So far we kept it really stupid by creating a wrapper around Go slices. But we need to fix this
one in the future.

- Since we didn't write the reader in Serene itself, there is no reader macroes.

** New contributors
If you're a new contributor and you want to start working on the source code and don't know where to start,
just look through the source code for *TODO:* sections and the ~dev.org~ on the root for some higher level
TODOs.

In order to start reading the code start from the ~pkg/core/parser.go~ file which is the parser implementation
and then take a look at ~pkg/core/core.go~ and ~pkg/core/eval.go~. If you have any question feel free to ask them
either on the mailing list or the gitter/IRC channel.

** Development hint

*** Use Make... functions
In order to create a new value in any type use the designated Make function, for example: =MakeList=


** Setup development environment
*** Emacs
All you have to do is to install =LSP= and =gopls= that's it.

** Running the REPL
In order to run the REPL by compiling the code just do:

#+BEGIN_SRC bash
go run serene.go repl
#+END_SRC

There is a =--debug= parameter which enables more debug output (For example: read -> AST -> out data).
