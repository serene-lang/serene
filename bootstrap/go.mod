module serene-lang.org/bootstrap

go 1.15

require (
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e
	github.com/golangci/golangci-lint v1.35.2 // indirect
	github.com/gookit/color v1.3.5
	github.com/rjeczalik/pkgconfig v0.0.0-20190903131546-94d388dab445
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
)
